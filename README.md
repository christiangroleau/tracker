# Tracker

![Release](https://img.shields.io/badge/Release-1.0-blue?labelColor=grey&style=flat)
![Python](https://img.shields.io/badge/Python-3.7+-blue?labelColor=grey&style=flat)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)

Tracker is a minimal global navigation satellite system (GNSS) positional logger.

![map](map.png)

Supports the following features:

- Read [NMEA 0183](https://en.wikipedia.org/wiki/NMEA_0183) sentences from a serial port
- Acquire latitude, longitude, alititude and speed over ground (knots) through GGA and RMC sentences
- Persist positional data in a database (sqlite)

## Quickstart

Tracker dependencies can be installed via [pip](https://docs.python.org/3/installing/index.html)
```
pip install -r requirements.txt
```
and requires Python 3.7.0 or higher

##  Getting started

To run Tracker with default configuration

```
python tracker.py
```

### Configuration

Tracker supports a minimal set of configuration

**Port** is set using the following:

```
python tracker.py --port=/dev/tty.usbserial-1430
```

Default is `/dev/ttyUSB0`

**Log level** is set using the following:

```
python tracker.py --log=INFO
```

Default is `WARNING`

## Logging

Logs are written into a text file `tracker.log` according to the configured log level described in this table

LOG LEVEL ||
--- | --- |
DEBUG | -- |
INFO | Positional data | 
WARNING | Sentence parsing issues |
ERROR | Application faults leading to termination of tracking |

While running, the log may be followed by using `tail`

```
tail -f tracker.log
```

## Database

Positional data is stored into a [SQLite](https://www.sqlite.org/index.html) database in `tracker.db` 

```
sqlite3 tracker.db
> select * from positions;
```

id | timestamp | latitude | longitude | altitude | speed |
--- | --- | --- | --- | --- | --- |
1 | 2022-06-29T00:13:18Z | 13.4125 | 103.866667 | 65.0 | 0.00 |


### Notes about hardware

Tracker uses a GPS antenna [Adafruit Ultimate GPS with USB](https://www.adafruit.com/product/4279) attached to a [Raspberry Pi](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/). Other GPS modules streaming NMEA 0183 data to a serial port will most likely work just as well.

The device port may be different on your operating system. Search for the appropriate `/dev/tty* ` device.
