from dataclasses import dataclass


@dataclass
class Position:
    """Representation of GNSS positional data."""
    timestamp: str = ''
    latitude: float = 0.0
    longitude: float = 0.0
    altitude: float = 0.0
    speed: float = 0.0

    def values(self):
        return (self.timestamp, self.latitude, self.longitude, self.altitude, self.speed)

    def __repr__(self):
        return '{} lat: {:.6f} lon: {:.6f} alt: {} speed: {}'.format(self.timestamp, self.latitude, self.longitude, self.altitude, self.speed)
