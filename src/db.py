import sqlite3


class DB:
    """Minimalist database to store positional data."""
    def create_connection(self, db_file):
        return sqlite3.connect(db_file)

    def create_table(self, connection):
        cursor = connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS positions (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp TEXT, latitude REAL, longitude REAL, altitude REAL, speed REAL)''')
        connection.commit()

    def get_positions(self, connection):
        cursor = connection.cursor()
        
        return cursor.execute("SELECT latitude, longitude FROM positions").fetchall()

    def save_position(self, connection, position):
        sql = '''INSERT INTO positions (timestamp, latitude, longitude, altitude, speed) VALUES (?, ?, ?, ?, ?)'''
        
        cursor = connection.cursor()
        cursor.execute(sql, position)
        connection.commit()

        return cursor.lastrowid