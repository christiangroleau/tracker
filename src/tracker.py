import argparse
from dataclasses import dataclass
from datetime import datetime, timezone
import logging
import io
import sys

import pynmea2
import serial

from db import DB
from position import Position

DEFAULT_PORT = '/dev/ttyUSB0'
DEFAULT_LOG_LEVEL = 30  # 'WARNING' (see: https://docs.python.org/3/library/logging.html#levels)


@dataclass
class Options:
    """ Application options. Some configurable at the command line. """
    port: str = DEFAULT_PORT
    log_level: int = DEFAULT_LOG_LEVEL
    log_file: str = 'tracker.log'
    db_file: str = 'tracker.db'


def processArguments():
    parser = argparse.ArgumentParser(description = "A simple global navigation satellite system (GNSS) positional tracker")
    parser.add_argument("-p", "--port", metavar = "<port>", type = str, help = "The listening port receiving positional data (default: /dev/ttyUSB0)")
    parser.add_argument("-l", "--log", metavar = "<log level>", type = str, help = "Log level DEBUG, INFO, WARNING or ERROR (default: WARNING)")

    args = parser.parse_args()

    numeric_level = None
    if (args.log):
        numeric_level = getattr(logging, args.log.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError('Invalid log level: {}'.format(args.log))

    opts = Options()
    opts.port = DEFAULT_PORT if args.port is None else args.port
    opts.log_level = DEFAULT_LOG_LEVEL if numeric_level is None else numeric_level

    return opts


def read_serial(sio):
    """Read and buffer serial port stream. Yields once both GGA and RMC sentences are parsed for each position."""
    position = Position()
    complete = 0

    while True:
        today = datetime.now(timezone.utc).date()

        try:
            line = sio.readline()
            sentence = pynmea2.parse(line)

            if type(sentence) == pynmea2.types.talker.GGA:
                position.timestamp = '{}T{}Z'.format(today, sentence.timestamp)
                position.latitude = sentence.latitude
                position.longitude = sentence.longitude
                position.altitude = sentence.altitude
                complete += 1

            if type(sentence) == pynmea2.types.talker.RMC:
                position.speed = sentence.spd_over_grnd
                complete += 1

            if complete == 2:
                complete = 0
                yield position

        except serial.SerialException as e:
            logging.error('Device error: {}'.format(e))
            break

        except pynmea2.ParseError as e:
            logging.warning('Parse error: {}'.format(e))
            continue


def main():
    opts = processArguments()

    logging.basicConfig(filename = opts.log_file, format = '%(asctime)s -- %(levelname)s -- %(message)s', encoding = 'utf-8', level = opts.log_level)

    print('Tracking positions on {} ...'.format(opts.port))

    try:
        db = DB()
        with db.create_connection(opts.db_file) as conn, serial.Serial(opts.port, 9600, timeout = 5.0) as ser:
            db.create_table(conn)
            sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))
            position = read_serial(sio)
            for pos in position:
                logging.info(pos)
                db.save_position(conn, pos.values())
    
    except Exception as e:
        logging.error('Unexpected error: {}'.format(e))
        sys.exit(1)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nTerminating positional tracking...')
        sys.exit(0)
